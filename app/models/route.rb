class Route < ApplicationRecord
belongs_to :user
belongs_to :airport

    validates :id_airport, presence: true
    validates :id_user, presence: true
    validates :route_origine, presence: true
    validates :route_destinatination, presence: true
    validates :departure_time, presence: true
    validates :class, presence: true
    validates :price, presence: true
    validates :airline, presences: true

    def new_attributes
        {
          id: self.id,
          airport: self.airport,
          user: self.user,
          route_origine: self.route_origine,
          route_destination: self.route_destination,
          departure_time: self.departure_time,
          class: self.class,
          price: self.price,
          airline: self.airline,

          created_at: self.created_at,
        }
      end
    def other_comment_attributes
      {
        id: self.id,
        airport: self.airport,
        user: self.user,
        }
    end
end
