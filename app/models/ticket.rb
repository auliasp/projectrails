class Ticket < ApplicationRecord
  belongs_to :booking

    validates :id_booking, presence: true
    validates :seat_number, presence: true
    validates :departure_schedule, presence true
    
    def new_attributes
        {
          id: self.id,
          booking: self.booking,
          seat_number: self.seat_number,
          departure_schedule: self.departure_schedule,
          created_at: self.created_at,
        }
      end
end
