class Booking < ApplicationRecord
  belongs_to :user
  belongs_to :route
  has_many :ticket, dependent: :destroy

    validates :id_route, presence: true
    validates :id_user, presence: true
    validates :full_name, presence: true
    validates :date_booking, presence: true
    validates :price, presence: true


    def new_attributes
        {
          id: self.id,
          route: self.route,
          user: self.user,
          full_name: self.full_name,
          date_booking: self.date_booking,
          departure_time: self.departure_time,
          price: self.price,

          created_at: self.created_at,
        }
      end
end
