class User < ApplicationRecord
    has_secure_password

    has_many :route, dependent: :destroy
    has_many :booking, dependent: :destroy

    validates :name, presence: true,
    validates :dob, presence: true,
    validates :NIK, presence: true,
    validates :phone_number, presence: true, length: {  maximum: 15 },
                                format: { with: /\A[0-9]+\z/ },
                                uniqueness: true
    validates :email, presence: true, length: { maximum: 255 },
                                format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i },
                                uniqueness: true

    def new_attributes
        {
            id: self.id
            name: self.name,
            dob: self.dob,
            NIK: self.NIK,
            phone_number: self.phone_number,
            email: self.email,
            created_at: self.created_at,
        }
    end
end
