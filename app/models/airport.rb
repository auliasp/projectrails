class Airport < ApplicationRecord
belongs_to :route
has_many :comments, dependent: :destroy

   validates :name, presence: true,
   validates :city, presence: true,
   validates :airport_code, presence: true,


   def new_attributes 
    {
        id: self.id,
        name: self.name,
        city: self.city,
        airport_code: self.airport_code,
        created_at: self.created_at,
    }
    end
end
