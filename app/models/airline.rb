class Airline < ApplicationRecord

    validates :type, presence: true,
    validates :seat_number, presences: true,
    validates :airport, presences: true,
    
    def new_attributes
        {
          id: self.id,
          type: self.type,
          seat_number: self.seat_number,
          airport: self.airport,
          created_at: self.created_at,
        }
    end
end
