class CreateAirlines < ActiveRecord::Migration[7.0]
  def change
    create_table :airlines do |t|
      t.string :type
      t.string :seat_number
      t.string :airport
      
      t.timestamps
    end
  end
end
