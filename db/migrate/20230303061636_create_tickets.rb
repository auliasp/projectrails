class CreateTickets < ActiveRecord::Migration[7.0]
  def change
    create_table :tickets do |t|
      t.string :seat_number
      t.date :departure_schedule
      
      t.timestamps
    end
  end
end
