class CreateRoutes < ActiveRecord::Migration[7.0]
  def change
    create_table :routes do |t|
      t.integer :id_airport
      t.integer :id_user
      t.string :route_origine
      t.string :route_destination
      t.date :departure_time
      t.string :class
      t.string :price
      t.string :airline

      t.timestamps
    end
  end
end
