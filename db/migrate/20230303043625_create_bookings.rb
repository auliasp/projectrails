class CreateBookings < ActiveRecord::Migration[7.0]
  def change
    create_table :bookings do |t|
      t.integer :id_route
      t.integer :id_user
      t.string :full_name
      t.string :date_booking
      t.string :price

      t.timestamps
    end
  end
end
